JOSHUA G. ORBISO BSIT-3 | G1

CREATE DATABASE blog_db;
USE blog_db;

CREATE TABLE post(
    id INT NOT NULL AUTO_INCREMENT,
    author_id INT NOT NULL,
    title VARCHAR(500) NOT NULL,
    content VARCHAR(5000) NOT NULL,
    datetime_posted DATETIME NOT NULL,
    PRIMARY KEY(id)  
);

CREATE TABLE post_comments(
    id INT NOT NULL AUTO_INCREMENT,
    post_id INT NOT NULL,
    user_id INT NOT NULL,    
    content VARCHAR(5000) NOT NULL,
    datetime_commented DATETIME NOT NULL,
    PRIMARY KEY(id),   
    CONSTRAINT fk_post_comments_post_id
        FOREIGN KEY (post_id) REFERENCES post(id)
        ON UPDATE CASCADE
        ON DELETE RESTRICT,       
    CONSTRAINT fk_post_comments_user_id
        FOREIGN KEY (user_id) REFERENCES users(id)
        ON UPDATE CASCADE
        ON DELETE RESTRICT
);

CREATE TABLE post_likes(
    id INT NOT NULL AUTO_INCREMENT,
    post_id INT NOT NULL,
    user_id INT NOT NULL,    
    datetime_liked DATETIME NOT NULL,
    PRIMARY KEY(id),   
    CONSTRAINT fk_post_likes_post_id
        FOREIGN KEY (post_id) REFERENCES post(id)
        ON UPDATE CASCADE
        ON DELETE RESTRICT,       
    CONSTRAINT fk_post_likes_user_id
        FOREIGN KEY (user_id) REFERENCES users(id)
        ON UPDATE CASCADE
        ON DELETE RESTRICT
);

CREATE TABLE users(
    id INT NOT NULL AUTO_INCREMENT,
    email VARCHAR(100) NOT NULL,
    password VARCHAR(300) NOT NULL,
    datetime_created DATETIME NOT NULL,
    PRIMARY KEY(id)
);

3.
INSERT INTO users (email, password, datetime_created) VALUES ("johnsmith@gmail.com","passwordA","2021-01-01 01:00:00");
INSERT INTO users (email, password, datetime_created) VALUES ("juandelacruz@gmail.com","passwordB","2021-01-01 02:00:00");
INSERT INTO users (email, password, datetime_created) VALUES ("janesmith@gmail.com","passwordC","2021-01-01 03:00:00");
INSERT INTO users (email, password, datetime_created) VALUES ("mariadelacruz@gmail.com","passwordD","2021-01-01 04:00:00");
INSERT INTO users (email, password, datetime_created) VALUES ("johndoe@gmail.com","passwordE","2021-01-01 05:00:00");

INSERT INTO post (author_id, title, content, datetime_posted) VALUES (1,"First Code","Hello World!","2021-01-02 01:00:00");
INSERT INTO post (author_id, title, content, datetime_posted) VALUES (1,"Second Code","Hello Earth!","2021-01-02 02:00:00");
INSERT INTO post (author_id, title, content, datetime_posted) VALUES (2,"Third Code","Welcome to Mars!","2021-01-02 03:00:00");
INSERT INTO post (author_id, title, content, datetime_posted) VALUES (4,"Fourth Code","Bye bye solar system!","2021-01-02 04:00:00");

4. 
SELECT * FROM post WHERE author_id = 1;
 
5. 
SELECT email, datetime_created FROM users;

6.
UPDATE post SET content = "Hello to the people of the Earth!" WHERE content = "Hello Earth!";

7.
DELETE FROM users WHERE email = "johndoe@gmail.com";